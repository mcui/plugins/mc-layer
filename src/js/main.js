import '../css/main.scss';

// 代码着色插件
import McTinting from 'mc-tinting';
// 新建一个代码着色实例
new McTinting();

// 实例代码
import layer from '../../lib/mc-layer';

// 示例一
// let aaa = new Layer({
//   id: 'aaa',
//   el: '#layer_root'
// });

document.getElementById('btn1').onclick = function () {
  console.log(1);
  console.log(layer.show({
    url: '../aaa.html'
  }));
};

// 示例二
document.getElementById('btn2').onclick = function () {
  console.log(2);

};
