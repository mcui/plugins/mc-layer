/**
 * o.area      {String}     指定弹出层代码的插入区块
 * o.code      {String}     要插入的DOM代码
 * o.dark      {Boolean}    是否显示暗层
 * o.load      {String}     自定义加载中
 * o.theme     {String}     显示或隐藏时的动画
 * o.show      {Function}   显示弹出层
 * o.hide      {Function}   隐藏弹出层
 * */

let McLayer = (param) => {

};

McLayer.prototype.show = () => {

};

McLayer.prototype.hide = () => {

};

// class McLayer {
//   constructor(o) {
//     this.error = o.error || '<div class="mc-layer-err">内容加载失败!</div>';
//     this.area  = o.area || document.body;
//     this.load  = o.load || '<div class="am-load-aa"></div>';
//     this.code  = toDom(o.code) || toDom(this.error);
//     this.case  = toDom(`<div class="mc-layer load" data-theme="aa" data-dark="true"><div class="mc-layer-warp">${this.load}</div></div>`);
//     this.wrap  = this.case.children[0];
//     console.log(this);
//   }
//
//   show(o) {
//
//     let amStatus = (e) => {
//       if (e.target === this.case) {
//
//       }
//       if (e.target === this.wrap) {
//         if (this.flow === 'load') {
//           this.wrap.innerHTML = '';
//           this.wrap.appendChild(this.code);
//           reClass(this.case, 'edit', 'show');
//           this.flow = 'show';
//         }
//         if (this.flow === 'hide') {
//           setTimeout(() => {
//             this.case.remove();
//             // this.case.removeEventListener(amEnd, amStatus, false);
//           }, 300);
//         }
//       }
//     };
//
//     this.case.addEventListener(amEnd, amStatus, false);
//     this.flow = 'load';
//     this.area.appendChild(this.case);
//     this.next = () => {
//       reClass(this.case, 'load', 'edit');
//     };
//     setTimeout(() => {
//       o && o.prep ? o.prep(this) : this.next();
//     }, 50);
//   }
//
//   hide() {
//     this.flow = 'hide';
//     reClass(this.case, 'show', 'hide');
//   }
// }
//
// // 动画结束后执行事件
// let amEnd = (function () {
//   let evs = {
//     WebkitAnimation: 'webkitAnimationEnd',
//     animation      : 'animationend'
//   };
//   for (let k in evs) {
//     if (typeof evs[k] === 'string') {
//       return evs[k];
//     }
//   }
// })();
//
// // 字符串创建DOM节点
// let toDom = function (arg) {
//   let dom       = document.createElement('div');
//   dom.innerHTML = arg;
//   return dom.children[0];
// };
//
// // 替换CLASS
// let reClass = function (dom, oldClass, newClass) {
//   dom.className = dom.className.replace(new RegExp(oldClass), newClass);
// };

export default McLayer;
